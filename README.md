# IronSquad phpBB theme

Кастомизированная версия темы [Black](http://www.artodia.com/demo.php?demo=phpbb31&id=8) от [Arty](http://www.artodia.com/).

## Сборка

[Инструкция по сборке](http://www.artodia.com/phpbb-31-tutorials/compile-theme/) на сайте Artodia.

Для сборки темы используется [SASS compiler for phpBB 3.1](http://www.artodia.com/threads/sass-compiler-for-phpbb-3-1.2186/)